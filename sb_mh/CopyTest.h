#pragma once

#include <iostream>

class CopyTest
{
public:
	CopyTest(const int n, const int m)
	{
		init_array(n, m);

		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++) {
				arr[i][j] = rand() % 100;
			}
		}

		info = new std::string("Hello");
	}

	CopyTest(const CopyTest& other)
	{
		std::cout << "copy constructor";

		if (other.arr)
		{
			init_array(other.n, other.m);

			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < m; j++)
				{
					arr[i][j] = other.arr[i][j];
				}
			}
		}

		if (other.info)
		{
			info = new std::string(*other.info);
		}
	}


	friend std::ostream& operator<< (std::ostream& out, const CopyTest& other);

	~CopyTest()
	{
		std::cout << "destructor";
		clear_array();
		delete info;
	}
private:
	int** arr;
	int n;
	int m;

	std::string* info;

	void init_array(int n, int m)
	{
		this->n = n;
		this->m = m;
		arr = new int* [n];
		for (int i = 0; i < n; ++i)
		{
			arr[i] = new int[m];
		}
	}

	void clear_array()
	{
		if (arr)
		{
			for (int i = 0; i < n; i++) {
				delete[] arr[i];
			}
			delete[] arr;
		}
	}
};

std::ostream& operator<< (std::ostream& out, const CopyTest& other)
{
	out << std::endl;
	for (int i = 0; i < other.n; i++)
	{
		for (int j = 0; j < other.m; j++) {
			out << other.arr[i][j] << " ";
		}
		out << std::endl;
	}
	out << *other.info << "\n\n";
	return out;
}
	