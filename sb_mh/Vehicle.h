#pragma once
#include <ostream>



class Wheel
{
public:
	Wheel(const float diameter) : diameter(diameter)
	{}

	float get_diameter()
	{
		return diameter;
	}
private:
	float diameter;
};

class Engine
{
public:
	Engine(const float power) : power(power)
	{}

	float get_power() const
	{
		return power;
	}

private:
	float power;
};

class Vehicle
{
public:
	virtual std::ostream& print(std::ostream& out) = 0;

	friend std::ostream& operator<<(std::ostream& out, Vehicle& vehicle)
	{
		return vehicle.print(out);
	}

	virtual ~Vehicle() = default;
};

class WaterVehicle : public Vehicle
{
public:
	WaterVehicle(float draft) : draft_(draft)
	{
	}

	std::ostream& print(std::ostream& out) override
	{
		return out;
	}

	~WaterVehicle() override = default;

private:
	float draft_;
};

class RoadVehicle : public Vehicle
{
public:
	RoadVehicle(float ground_clearance) : ground_clearance_(ground_clearance)
	{
	}

	std::ostream& print(std::ostream& out) override
	{
		out << "Ride height: " << ground_clearance_;
		return out;
	}

	~RoadVehicle() override = default;

private:
	float ground_clearance_;
};

class Bicycle : public RoadVehicle
{
public:
	Bicycle(Wheel forward, Wheel backward, float ground_clearance)
	: RoadVehicle(ground_clearance), forward_(forward), backward_(backward)
	{}

	std::ostream& print(std::ostream& out) override
	{
		out << "Bicycle" << " ";
		out << "Wheels: " << forward_.get_diameter() << " " << backward_.get_diameter() << " ";
		RoadVehicle::print(out);

		return out;
	}

	~Bicycle() override = default;

private:
	Wheel forward_;
	Wheel backward_;
};

class Car : public RoadVehicle
{
public:
	Car(Engine engine, Wheel top_left_wheel, Wheel top_right_wheel, Wheel bottom_left_wheel, Wheel bottom_right_wheel, float ground_clearance)
		: RoadVehicle(ground_clearance), engine_(engine), top_left_wheel_(top_left_wheel),
	top_right_wheel_(top_right_wheel), bottom_left_wheel_(bottom_left_wheel), bottom_right_wheel_(bottom_right_wheel)
	{}

	std::ostream& print(std::ostream& out) override
	{
		out << "Car ";
		out << "Engine: " << engine_.get_power() << " ";
		out << "Wheels: " << top_left_wheel_.get_diameter() << " " << top_right_wheel_.get_diameter() << " " << bottom_left_wheel_.get_diameter() << " " << bottom_right_wheel_.get_diameter() << " ";
		RoadVehicle::print(out);

		return out;
	}

	float get_power()
	{
		return engine_.get_power();
	}

	~Car() override = default;

private:
	Engine engine_;

	Wheel top_left_wheel_;
	Wheel top_right_wheel_;
	Wheel bottom_left_wheel_;
	Wheel bottom_right_wheel_;
};