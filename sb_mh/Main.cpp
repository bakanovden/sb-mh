﻿#include <iostream>
#include <cmath>
#include <vector>

#include "Vector.h"
#include "CopyTest.h"
#include "Vehicle.h"

int add(int a, int b)
{
    return a + b;
}

int subtract(int a, int b)
{
    return a - b;
}

int multiply(int a, int b)
{
    return a * b;
}

void lesson3()
{
    // start 3 lesson ----------------------------------

    int a;
    std::cout << "Enter a number: ";
    std::cin >> a;

    int b;
    std::cout << "Enter another number: ";
    std::cin >> b;

    int op;
    do
    {
        std::cout << "Enter an operation (0 = add, 1 = subtract, 2 = multiply): ";
        std::cin >> op;
    } while (op < 0 || op > 2);

    int (*pFnc)(int, int) = nullptr;
    switch (op)
    {
    case 0: pFnc = add; break;
    case 1: pFnc = subtract; break;
    case 2: pFnc = multiply; break;
    }

    std::cout << "The answer is : " << pFnc(a, b) << std::endl;


    // end 3 lesson ----------------------------------
}

void homework2()
{
    // start 2 ----------------------------------

	CopyTest* test = new CopyTest(10, 10);
	std::cout << *test;

	CopyTest* test2 = new CopyTest(*test);
	std::cout << *test2;

	delete test;
	std::cout << *test2;
    delete test2;


	// end 2 ----------------------------------
	
}

void homework4_1()
{
    Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);
    std::cout << c << '\n';



    Bicycle t(Wheel(20), Wheel(20), 300);
    std::cout << t << '\n';
}

float getHighestPower(std::vector<Vehicle*> v)
{
    float max_power = 0.0f;
    for (Vehicle* vehicle : v)
    {
        Car* carRef = dynamic_cast<Car*>(vehicle);
        if (carRef != nullptr)
        {
            if (carRef->get_power() > max_power)
            {
                max_power = carRef->get_power();
            }
        }
    }

    return max_power;
}

void homework4_2()
{
    std::vector<Vehicle*> v;
    v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));
    v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));
    v.push_back(new WaterVehicle(5000));



    for (Vehicle* vehicle : v)
    {
        std::cout << *vehicle << '\n';
    }



    std::cout << "The highest power is " << getHighestPower(v) << '\n'; // реализуйте эту функцию

    for (const Vehicle* vehicle : v)
    {
        delete vehicle;
    }
    v.clear();
}

int main()
{
	// start 1 ----------------------------------
    // Vector v1(0, 1, 2);
    // Vector v2(3, 4, 5);
    // std::cout << v1 + v2 << '\n';
    // std::cout << v1 * 5.0f << '\n';
    // std::cout << v1 * v2 << '\n';
    // std::cout << v1 - v2 << '\n';
    // std::cin >> v1;
    // std::cout << v1 << '\n';
    // end 1 ----------------------------------


    homework4_1();
    homework4_2();


    return 0;
}