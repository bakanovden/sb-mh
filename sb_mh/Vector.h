#pragma once
#include <iostream>
#include <cmath>

class Vector
{
public:
    Vector()
    {
        this->x = 0;
        this->y = 0;
        this->z = 0;
    }

    //запрещает неявное преобразование типов (v = flaot)
    explicit Vector(const float value)
    {
        this->x = value;
        this->y = value;
        this->z = value;
    }

    //запрещает инициализировать одним числом например
    Vector(int value) = delete;

    Vector(const float x, const float y, const float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    Vector(const Vector& other)
    {
        std::cout << "copy constructor";
        this->x = other.x;
        this->y = other.y;
        this->z = other.z;
    }

    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    }

    Vector& operator= (const Vector& other)
    {
        this->x = other.x;
        this->y = other.y;
        this->z = other.z;

        return (*this);
    }

    friend Vector operator+ (const Vector& a, const Vector& b);

    friend Vector operator- (const Vector& a, const Vector& b);

    friend Vector operator* (const Vector& a, const Vector& b);

    friend Vector operator* (const Vector& a, float b);

    friend std::ostream& operator<< (std::ostream& out, const Vector& v);

    friend std::istream& operator>> (std::istream& out, Vector& v);

    friend bool operator> (const Vector& a, const Vector& b);

    float operator[] (int index)
    {
        switch (index)
        {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return y;
        default:
            std::cout << "index error";
            return 0;
        }
    }

private:
    float x;
    float y;
    float z;
};

Vector operator+ (const Vector& a, const Vector& b)
{
    return { a.x + b.x, a.y + b.y, a.z + b.z };
}

Vector operator- (const Vector& a, const Vector& b)
{
    return { a.x - b.x, a.y - b.y, a.z - b.z };
}

Vector operator* (const Vector& a, const Vector& b)
{
    return { a.x * b.x, a.y * b.y, a.z * b.z };
}

Vector operator* (const Vector& a, float b)
{
    return { a.x * b, a.y * b, a.z * b };
}

std::ostream& operator<< (std::ostream& out, const Vector& v)
{
    out << ' ' << v.x << ' ' << v.y << ' ' << v.z;
    return out;
}

std::istream& operator>> (std::istream& in, Vector& v)
{
    in >> v.x;
    in >> v.y;
    in >> v.z;

    return in;
}

bool operator> (const Vector& a, const Vector& b)
{
    return false;
}

