#include <iostream>
#include <vector>
#include <functional>

class Parent
{
protected:
	int m_value;

public:
	Parent (int value) : m_value(value)
	{
	}

	virtual const char* getName() const { return "Parent"; }
	int getValue() const { return m_value; }
};

class Child: public Parent
{
protected:
	int m_value;

public:
	Child(int value) : Parent(value)
	{
	}

	virtual const char* getName() const override { return "Child"; }
};

int main_2()
{
	std::vector<std::reference_wrapper<Parent>> v;

	Parent p(7);
	Child ch(8);
	v.push_back(p);
	v.push_back(ch);

	for (auto& i : v)
	{
		std::cout << "I am a " << i.get().getName() << " with value " << i.get().getValue() << "\n";
	}

	return 0;
}