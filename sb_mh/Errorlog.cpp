//interface
class IErrorLog
{
public:
	virtual bool openLog(const char* filename) = 0;
	virtual bool closeLog() = 0;
	virtual bool writeLog(const char* error_message) = 0;

	virtual ~IErrorLog() {};
};